package com.example.encryption;

import java.util.HashMap;

public interface EncryptionUtil {
    String encryptString(String plainString) throws Exception;
    String decryptString(String encryptedString) throws Exception;

}
