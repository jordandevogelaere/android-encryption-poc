package com.example.encryption

import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.support.annotation.RequiresApi
import android.util.Base64
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.GCMParameterSpec


@RequiresApi(Build.VERSION_CODES.M)
class SharedPreferencesNewEncryptor {
    private var keyStore: KeyStore
    private var ivBytes: ByteArray? = null

    init {
        keyStore = KeyStore.getInstance(AndroidKeyStore)
        keyStore.load(null)

        if (!keyStore.containsAlias(KEY_ALIAS)) {
            val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore)
            keyGenerator.init(
                KeyGenParameterSpec.Builder(
                    KEY_ALIAS,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM).setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .setRandomizedEncryptionRequired(true)
                    .build()
            )
            keyGenerator.generateKey()
        }
    }

    fun encrypt(input: ByteArray): String {
        /*  val c = Cipher.getInstance("AES/GCM/NoPadding")
          c.init(Cipher.ENCRYPT_MODE, secretKey)
          val encodedBytes = c.doFinal(input)
          return Base64.encodeToString(encodedBytes, Base64.DEFAULT)*/

        //Get the key
        val keyStore = KeyStore.getInstance("AndroidKeyStore")
        keyStore.load(null)
        val secretKeyEntry = keyStore.getEntry(KEY_ALIAS, null) as KeyStore.PrivateKeyEntry
        val secretKey = secretKeyEntry.privateKey


        //Encrypt data
        val cipher = Cipher.getInstance("RSA/ECB/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        val encryptedBytes = cipher.doFinal(input)

        return Base64.encodeToString(encryptedBytes, Base64.DEFAULT)
    }

    fun decrypt(encrypted: ByteArray): ByteArray {
        /* val c = Cipher.getInstance("AES/GCM/NoPadding")
         c.init(Cipher.DECRYPT_MODE, secretKey)
         val decordedValue = android.util.Base64.decode(encrypted, 0)
         return c.doFinal(decordedValue)*/

        //Get the key
        val keyStore = KeyStore.getInstance("AndroidKeyStore")
        keyStore.load(null)
        val secretKeyEntry = keyStore.getEntry(KEY_ALIAS, null) as KeyStore.PrivateKeyEntry
        val secretKey = secretKeyEntry.privateKey

        //Decrypt data
        val cipher = Cipher.getInstance("RSA/ECB/PKCS5Padding")
        cipher.init(Cipher.DECRYPT_MODE, secretKey)
        val decordedValue = android.util.Base64.decode(encrypted, 0)

        return cipher.doFinal(decordedValue)
    }


    companion object {
        private const val KEY_ALIAS = "KEY_ALIAS"
        const val AES_MODE = "AES/CBC/PKCS7Padding"
        const val AndroidKeyStore = "AndroidKeyStore"
    }
}