package com.example.encryption;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class MarshmallowEncryptionUtil implements EncryptionUtil {

    private Cipher cipher;
    private byte[] currentIv;

    public MarshmallowEncryptionUtil() throws NoSuchPaddingException, NoSuchAlgorithmException {
        cipher = Cipher.getInstance("AES/GCM/NoPadding");
    }

  /*  @Override
    public HashMap<String, byte[]> encrypt(byte[] decryptedBytes) throws Exception {
        final HashMap<String, byte[]> map = new HashMap<>();

        //Get the key
        final KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore.getEntry("MyKeyAlias", null);
        final SecretKey secretKey = secretKeyEntry.getSecretKey();

        //Encrypt data
        final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        final byte[] ivBytes = cipher.getIV();
        final byte[] encryptedBytes = cipher.doFinal(decryptedBytes);
        map.put("iv", ivBytes);
        map.put("encrypted", encryptedBytes);

        return map;
    }

    @Override
    public byte[] decrypt(HashMap<String, byte[]> map) throws Exception {
        byte[] decryptedBytes;
        //Get the key
        final KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore.getEntry("MyKeyAlias", null);
        final SecretKey secretKey = secretKeyEntry.getSecretKey();

        //Extract info from map
        final byte[] encryptedBytes = map.get("encrypted");
        final byte[] ivBytes = map.get("iv");

        //Decrypt data
        final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        final GCMParameterSpec spec = new GCMParameterSpec(128, ivBytes);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, spec);
        decryptedBytes = cipher.doFinal(encryptedBytes);


        return decryptedBytes;
    }*/

    @Override
    public String encryptString(String plainString) throws Exception {

        //Get the key
        final KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore.getEntry("MyKeyAlias", null);
        final SecretKey secretKey = secretKeyEntry.getSecretKey();

        //Encrypt data
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        currentIv = cipher.getIV();
        final byte[] encryptedBytes = cipher.doFinal(plainString.getBytes());

        return android.util.Base64.encodeToString(encryptedBytes, android.util.Base64.DEFAULT);
    }

    @Override
    public String decryptString(String encryptedString) throws Exception {
        byte[] decryptedBytes;
        //Get the key
        final KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore.getEntry("MyKeyAlias", null);
        final SecretKey secretKey = secretKeyEntry.getSecretKey();

        //Decrypt data

        final GCMParameterSpec spec = new GCMParameterSpec(128, currentIv);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, spec);
        decryptedBytes = cipher.doFinal(encryptedString.getBytes());


        return android.util.Base64.encodeToString(decryptedBytes, android.util.Base64.DEFAULT);
    }
}
