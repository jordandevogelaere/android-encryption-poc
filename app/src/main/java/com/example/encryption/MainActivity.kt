package com.example.encryption

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import be.introlution.common.SharedPreferencesEncryptor
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val prefs: SharedPreferences = baseContext.getSharedPreferences("test", 0)
        val encryptor = SharedPreferencesEncryptor(baseContext)

        val password = "JORDAN"

        val encrypted = encryptor.encrypt(baseContext, password.toByteArray())
        val editor = prefs.edit()
        editor.putString("password", encrypted)
        editor.apply()


        val decryptedKey = prefs.getString("password", null)

        val test = String(encryptor.decrypt(baseContext, decryptedKey.toByteArray()))

        hello_world.text = test

        //newEncryptor()
       // testEncryptor()
        //SPEncryption(baseContext).testEncryption()
        TestEncryption(baseContext).testEncryption()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun newEncryptor(){

        val prefs: SharedPreferences = baseContext.getSharedPreferences("test", 0)
        val domain = "JIDOKA"

        val encryptor = SharedPreferencesNewEncryptor()

        val encryptedDomain = encryptor.encrypt(domain.toByteArray())

        val editor = prefs.edit()
        editor.putString("domain", encryptedDomain)
        editor.apply()

        val decryptedKey = prefs.getString("domain", null)

        val test = String(encryptor.decrypt(decryptedKey.toByteArray()))

        hello_world.text = test

    }

    private fun testEncryptor(){
       TestEncryption(baseContext).testEncryption()
    }
}
