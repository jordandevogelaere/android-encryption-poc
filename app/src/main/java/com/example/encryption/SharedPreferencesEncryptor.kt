package be.introlution.common

import android.content.Context
import android.os.Build
import android.security.KeyPairGeneratorSpec
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.math.BigInteger
import java.security.Key
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.SecureRandom
import java.util.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.spec.SecretKeySpec
import javax.security.auth.x500.X500Principal
import kotlin.collections.ArrayList
import android.R.id.message




class SharedPreferencesEncryptor// Generate a key pair for encryption// Generate the RSA key pairs
    (context: Context) {

    companion object {
        const val ANDROID_KEYSTORE = "AndroidKeyStore"
        const val KEY_ALIAS = "KEY_ALIAS"
        const val RSA_MODE = "RSA/ECB/PKCS1Padding"
        const val ENCRYPTED_KEY = "ENCRYPTED_KEY"
        const val AES_MODE = "AES/ECB/PKCS5Padding"
    }

    private var keyStore: KeyStore

    init {
        keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
        keyStore.load(null)
        if (!keyStore.containsAlias(KEY_ALIAS)) {
            // Generate a key pair for encryption
            val start = Calendar.getInstance()
            val end = Calendar.getInstance()
            end.add(Calendar.YEAR, 30)

            val keyGenParameterSpec = KeyPairGeneratorSpec.Builder(context)
                .setAlias(KEY_ALIAS)
                .setSubject(X500Principal("CN=" + KEY_ALIAS))
                .setSerialNumber(BigInteger.TEN)
                .setStartDate(start.time)
                .setEndDate(end.time)
                .build()

            val keyPairGenerator = KeyPairGenerator.getInstance("RSA", ANDROID_KEYSTORE)
            keyPairGenerator.initialize(keyGenParameterSpec)
            keyPairGenerator.genKeyPair()

            generateAndStoreAESKey(context)
        }
    }

    private fun generateAndStoreAESKey(context: Context) {
        val sharedPreferences = context.getSharedPreferences("test", 0)
        var encryptedKeyB64 = sharedPreferences.getString(ENCRYPTED_KEY, null)
        if (encryptedKeyB64 == null) {
            val key = ByteArray(16)
            val secureRandom = SecureRandom()
            secureRandom.nextBytes(key)

            val encryptedKey = rsaEncrypt(key)
            encryptedKeyB64 = android.util.Base64.encodeToString(encryptedKey, android.util.Base64.DEFAULT)
            val sharedPreferencesEditor = sharedPreferences.edit()
            sharedPreferencesEditor.putString(ENCRYPTED_KEY, encryptedKeyB64)
            sharedPreferencesEditor.apply()
        }
    }

    private fun rsaEncrypt(secretByteArray: ByteArray): ByteArray {
        val privateKeyEntry = keyStore.getEntry(KEY_ALIAS, null) as KeyStore.PrivateKeyEntry

        //ENCRYPT THE TEXT
        val inputCipher = getCipher()
        inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.certificate.publicKey)

        val outputStream = ByteArrayOutputStream()
        val cipherOutputStream = CipherOutputStream(outputStream, inputCipher)
        cipherOutputStream.write(secretByteArray)
        cipherOutputStream.close()

        return outputStream.toByteArray()
    }

    private fun rsaDecrypt(encryptedByteArray: ByteArray): ByteArray {
        val privateKeyEntry = keyStore.getEntry(KEY_ALIAS, null) as KeyStore.PrivateKeyEntry

        val output = getCipher()
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.privateKey)
        val cipherInputStream = CipherInputStream(
            ByteArrayInputStream(encryptedByteArray), output)

        val values = ArrayList<Byte>()

        var nextByte = 0
        while (nextByte != -1) {
            nextByte = cipherInputStream.read()
            if (nextByte != -1) {
                values.add(nextByte.toByte())
            }
        }

        val bytes = ByteArray(values.count())
        for (i in bytes.indices) {
            bytes[i] = values[i]
        }

        return bytes
    }

    @Throws(Exception::class)
    private fun getSecretKey(context: Context): Key {
        val sharedPreferences = context.getSharedPreferences("test", 0)
        val enryptedKeyB64 = sharedPreferences.getString(ENCRYPTED_KEY, null)
        // need to check null, omitted here
        val encryptedKey = android.util.Base64.decode(enryptedKeyB64, android.util.Base64.DEFAULT)
        val key = rsaDecrypt(encryptedKey)
        return SecretKeySpec(key, AES_MODE)
    }

    private fun getCipher(): Cipher {
        try {
            return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) { // below android m
                Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL") // error in android 6: InvalidKeyException: Need RSA private or public key
            } else { // android m and above
                Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidKeyStoreBCWorkaround") // error in android 5: NoSuchProviderException: Provider not available: AndroidKeyStoreBCWorkaround
            }
        } catch (exception: Exception) {
            throw RuntimeException("getCipher: Failed to get an instance of Cipher", exception)
        }
    }

    fun encrypt(context: Context, input: ByteArray): String {
        val c = Cipher.getInstance(AES_MODE, "BC")
        c.init(Cipher.ENCRYPT_MODE, getSecretKey(context))
        val encodedBytes = c.doFinal(input)
        return android.util.Base64.encodeToString(encodedBytes, android.util.Base64.DEFAULT)
    }

    fun decrypt(context: Context, encrypted: ByteArray): ByteArray {
        val c = Cipher.getInstance(AES_MODE, "BC")
        c.init(Cipher.DECRYPT_MODE, getSecretKey(context))

        val decordedValue = android.util.Base64.decode(encrypted, 0)
        return c.doFinal(decordedValue)
    }

}