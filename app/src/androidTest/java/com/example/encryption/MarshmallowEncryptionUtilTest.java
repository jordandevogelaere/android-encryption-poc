package com.example.encryption;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class MarshmallowEncryptionUtilTest {

    @Test
    public void canEncryptAndDecryptString() throws Exception {
        String domain = "JIDOKA";
        MarshmallowEncryptionUtil encryptionUtil = new MarshmallowEncryptionUtil();

        String encryptedDomain = encryptionUtil.encryptString(domain);

        String decryptedDomain = encryptionUtil.decryptString(encryptedDomain);

        assertEquals(domain, decryptedDomain);
    }

    @Test
    public void canAddToSharedPreferencesAndEncryptAndDecryptString() throws Exception {
       /* String domain = "JIDOKA";

        Context context = InstrumentationRegistry.getContext();

        SharedPreferences sharedPreferences = context.getSharedPreferences("Test", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        MarshmallowEncryptionUtil encryptionUtil = new MarshmallowEncryptionUtil();

        HashMap<String, byte[]> encryptedDomain = encryptionUtil.encrypt(domain.getBytes());
        String encryptedDomainString =  android.util.Base64.encodeToString(encryptedDomain.get("encrypted"), android.util.Base64.DEFAULT);
        String encryptedIvString = android.util.Base64.encodeToString(encryptedDomain.get("iv"), android.util.Base64.DEFAULT);
        editor.putString("domain", encryptedDomainString);
        editor.putString("iv", encryptedIvString);
        editor.apply();

        String encryptedSharedPreferencesDomain = sharedPreferences.getString("domain", null);
        String encryptedSharedPreferencesIv = sharedPreferences.getString("iv", null);

        final HashMap<String, byte[]> map = new HashMap<>();
        map.put("encrypted", encryptedSharedPreferencesDomain.getBytes(StandardCharsets.UTF_8));
        map.put("iv", encryptedSharedPreferencesIv.getBytes(StandardCharsets.UTF_8));

        byte[] decryptedDomain = encryptionUtil.decrypt(map);

        String decryptedDomainString = new String(decryptedDomain, StandardCharsets.UTF_8);

        assertEquals(domain, decryptedDomainString);*/
    }



}
